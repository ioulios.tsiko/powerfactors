# POWERFACTOR ASSIGNMENT

[POSTMAN Documentation](https://documenter.getpostman.com/view/1990318/2s93sabZ3c)

## Run
Powerfactor binary creates an http server on port 80 by default. To run on diferent port provide the port number including the ":" as the first command-line argument
ex.  
Listen in default port 80
```./powerfactors :8080```

Listen in given port 8080
```./powerfactors :8080```

### Run localy
Run localy and listen on port 8080
```
go mod download
go build

./powerfactors :8080
```
### Run in docker container

Run in docker container and listen on port 8080
```
docker build -t powerfactors .
docker run -p 80:8080 powerfactors :8080
```


## API

```GET /plist```  
Query params
* **period** : Period of periodic task. Accepted values: 1h, 1d, 1mo, 1y.
* **tz**: Timezone. Timezone should be IANA Time Zone database compatible. 
* **t1**: Starting time point of the periodic task 
* **t2**: Ending time point of the periodic task 


### Run with Curl
```
curl --location 'localhost/ptlist?period=1y&tz=Europe%2FAthens&t1=20180214T204603Z&t2=20211115T123456Z'
```