package handler

import (
	"net/http"
	"time"

	"github.com/labstack/echo/v4"
	"gitlab.com/ioulios.tsiko/powerfactors/task"
)

type Handler struct {
	t task.TaskInterface
}

func NewHandler(t task.TaskInterface) *Handler {
	return &Handler{t: t}
}

// period=1h&tz=Europe/Athens&t1=20210714T204603Z&t2=20210715T123456Z
func (h *Handler) PtlistHandler(c echo.Context) error {
	var (
		period task.Period
		tz     time.Location
		t1     time.Time
		t2     time.Time
	)

	req := new(PtlistReq)

	///Bind validate request parameters
	if err := req.bind(c, &period, &tz, &t1, &t2); err != nil {
		return c.JSON(http.StatusBadRequest, err)
	}

	ptlist := h.t.GetTimestamps(period, &tz, t1, t2)

	return c.JSON(http.StatusOK, ptlist)
}

// ptlist Request parameters
type PtlistReq struct {
	Period   string `query:"period"`
	Timezone string `query:"tz"`
	T1       string `query:"t1"`
	T2       string `query:"t2"`
}

// Error response
type ErrorResp struct {
	Status          string            `json:"status"`
	ValidationError []ValidationError `json:"validation_error"`
}

// Validation Error
type ValidationError struct {
	Key         string `json:"key"`
	Description string `json:"descr"`
}

// Bind and validate http request params
func (ptlr *PtlistReq) bind(c echo.Context, period *task.Period, tz *time.Location, t1 *time.Time, t2 *time.Time) *ErrorResp {
	if err := c.Bind(ptlr); err != nil {
		return &ErrorResp{Status: "error"}
	}

	ve := []ValidationError{}

	// parse period
	if p, err := task.ParsePeriod(ptlr.Period); err != nil {
		ve = append(ve, ValidationError{Key: "period", Description: err.Error()})
	} else {
		*period = *p
	}

	// Load location, return error if given timezone is empty or invalid
	if loc, err := time.LoadLocation(ptlr.Timezone); err != nil || ptlr.Timezone == "" {
		ve = append(ve, ValidationError{Key: "tz", Description: "Invalid timezone"})
	} else {
		*tz = *loc
	}

	// Parse time t1
	if tm, err := time.Parse("20060102T150405Z", ptlr.T1); err != nil {
		ve = append(ve, ValidationError{Key: "t1", Description: "Invalid time"})
	} else {
		*t1 = tm
	}

	// Parse time t2
	if tm, err := time.Parse("20060102T150405Z", ptlr.T2); err != nil {
		ve = append(ve, ValidationError{Key: "t2", Description: "Invalid time"})
	} else {
		*t2 = tm
	}

	// If are validaton error, return ErrorResp
	if len(ve) > 0 {
		return &ErrorResp{Status: "error", ValidationError: ve}
	}

	return nil
}
