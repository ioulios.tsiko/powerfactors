package handler_test

import (
	"encoding/json"
	"net/http"
	"net/http/httptest"
	"reflect"
	"testing"

	"github.com/labstack/echo/v4"
	"gitlab.com/ioulios.tsiko/powerfactors/handler"
	"gitlab.com/ioulios.tsiko/powerfactors/task"
)

func TestPtlistHandler(t *testing.T) {
	mt := &task.MockTask{GetTimestampsCalled: false}
	h := handler.NewHandler(mt)

	validationError := handler.ErrorResp{
		Status: "error",
		ValidationError: []handler.ValidationError{
			{
				Key:         "period",
				Description: "Invalid period. Expected values: 1h, 1d, 1mo, 1y",
			},
			{
				Key:         "tz",
				Description: "Invalid timezone",
			},
			{
				Key:         "t1",
				Description: "Invalid time",
			},
			{
				Key:         "t2",
				Description: "Invalid time",
			},
		},
	}

	t.Run("get ptlist successfully", func(t *testing.T) {
		c, _, res := newGetRequest("/ptlist?period=1h&tz=Europe/Athens&t1=20210714T204603Z&t2=20210715T123456Z")

		h.PtlistHandler(c)
		assertHTTPStatus(t, res.Code, http.StatusOK)

		var got []string
		json.NewDecoder(res.Body).Decode(&got)

		assertEqual(t, got, []string{})
		assertFunctionCalled(t, mt.GetTimestampsCalled, true, "GetTimestamps")
	})

	t.Run("get ptlist with invalid query params", func(t *testing.T) {
		mt.GetTimestampsCalled = false // seting GetTimestampsCalled to false
		c, _, res := newGetRequest("/ptlist")

		h.PtlistHandler(c)
		assertHTTPStatus(t, res.Code, http.StatusBadRequest)

		var got handler.ErrorResp
		json.NewDecoder(res.Body).Decode(&got)

		assertEqual(t, got, validationError)
		assertFunctionCalled(t, mt.GetTimestampsCalled, false, "GetTimestamps")
	})

}

func newGetRequest(url string) (c echo.Context, request *http.Request, response *httptest.ResponseRecorder) {
	e := echo.New()

	request, _ = http.NewRequest(http.MethodGet, url, nil)

	response = httptest.NewRecorder()
	c = e.NewContext(request, response)

	return
}

func assertError(t testing.TB, err error, expected bool) {
	t.Helper()
	if expected && err == nil {
		t.Errorf("expected to get an error")
	} else if !expected && err != nil {
		t.Errorf("not expected to get an error but got '%s'", err.Error())
	}
}

func assertHTTPStatus(t testing.TB, got, want int) {
	t.Helper()
	if got != want {
		t.Errorf("wanted http status %d but got http status %d", want, got)
	}
}

func assertEqual(t testing.TB, got, want interface{}) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("wanted %q \nbut got %q", want, got)
	}
}

func assertFunctionCalled(t testing.TB, called, want bool, funcName string) {
	t.Helper()
	if called != want {
		t.Errorf("wanted to call method '%s' but didn't, %t", funcName, called)
	}
}
