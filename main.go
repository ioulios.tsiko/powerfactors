package main

import (
	"os"
	"regexp"

	"github.com/labstack/echo/v4"
	"gitlab.com/ioulios.tsiko/powerfactors/handler"
	"gitlab.com/ioulios.tsiko/powerfactors/task"
)

func main() {
	e := echo.New()
	port := ":80"

	// reading listening port from first argumen. If port is not valid, stop executing
	if len(os.Args) > 1 {
		pre := regexp.MustCompile(`^[:]\d{1,5}$`)
		isValidPort := pre.MatchString(os.Args[1])
		if isValidPort {
			port = os.Args[1]
		} else {
			e.Logger.Fatal("You need to provide a valid port. Default :80")
		}
	}

	t := task.NewTask()
	h := handler.NewHandler(t)

	e.GET("/ptlist", h.PtlistHandler)
	e.Logger.Fatal(e.Start(port))
}
