package task

import (
	"errors"
	"regexp"
	"strconv"
	"time"
)

type TaskInterface interface {
	GetTimestamps(period Period, tz *time.Location, t1 time.Time, t2 time.Time) []string
}

type Task struct {
}

func NewTask() *Task {
	return &Task{}
}

func (t *Task) GetTimestamps(p Period, tz *time.Location, t1 time.Time, t2 time.Time) []string {
	tm := t1.In(tz)
	endTime := t2.In(tz)
	ptlist := []string{}

	// Set the invocation timestamp in the start of the period
	tm = getInvocationTime(tm, p.Type)

	for tm.Before(endTime) {
		ptlist = append(ptlist, tm.UTC().Format("20060102T150405Z"))
		switch p.Type {
		case "h":
			tm = tm.Add(time.Duration(p.Value) * time.Hour)
		case "d":
			tm = tm.AddDate(0, 0, p.Value)
		case "mo":
			tm = tm.AddDate(0, p.Value, 0)
		case "y":
			tm = tm.AddDate(p.Value, 0, 0)
		}
	}

	return ptlist
}

func getInvocationTime(tm time.Time, periodType string) time.Time {
	switch periodType {
	case "h":
		// truncate to the nearest hour
		return tm.Truncate(time.Hour).Add(time.Hour)
	case "d":
		year, month, day := tm.Date()
		nextDay := time.Date(year, month, day+1, 0, 0, 0, 0, tm.Location())
		return nextDay
	case "mo":
		year, month, _ := tm.Date()
		nextMonth := time.Date(year, month+1, 1, 0, 0, 0, 0, tm.Location())
		return nextMonth
	case "y":
		year, _, _ := tm.Date()
		nextYear := time.Date(year+1, time.January, 1, 0, 0, 0, 0, tm.Location())
		return nextYear
	}
	return tm
}

type Period struct {
	Type  string
	Value int
}

var PeriodTypes = []string{"h", "d", "mo", "y"}
var PeriodTypeError = errors.New("Invalid period. Expected values: 1h, 1d, 1mo, 1y")

func ParsePeriod(p string) (*Period, error) {
	// Regular expression pattern to match period
	pattern := `^(\d{1,2})([A-Za-z]{1,2})$`
	regex := regexp.MustCompile(pattern)

	// Check if the period string matches period format
	if regex.MatchString(p) {
		// Split the period value and period type
		submatches := regex.FindStringSubmatch(p)
		val, err := strconv.Atoi(submatches[1])
		if err != nil {
			return nil, PeriodTypeError
		}
		tp := submatches[2]

		return &Period{Type: tp, Value: val}, nil
	}
	return nil, PeriodTypeError
}

// Check if period is a valid type
func IsPeriodType(tp string) bool {
	for _, p := range PeriodTypes {
		if p == tp {
			return true
		}
	}
	return false
}

func truncateToHour(t time.Time) time.Time {
	nt := time.Date(t.Year(), t.Month(), t.Day(), t.Hour(), 0, 0, 0, t.Location()).Add(time.Hour)
	return nt
}

func truncateToDay(t time.Time) time.Time {
	nt := time.Date(t.Year(), t.Month(), t.Day(), 0, 0, 0, 0, t.Location()).AddDate(0, 0, 1)
	return nt
}

func truncateToMonth(t time.Time) time.Time {
	nt := time.Date(t.Year(), t.Month(), 0, 0, 0, 0, 0, t.Location()).AddDate(0, 1, 0)
	return nt
}

func truncateToYear(t time.Time) time.Time {
	nt := time.Date(t.Year(), 0, 0, 0, 0, 0, 0, t.Location()).AddDate(1, 0, 0)
	return nt
}
