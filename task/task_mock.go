package task

import "time"

type MockTask struct {
	GetTimestampsCalled bool
}

func (t *MockTask) GetTimestamps(period Period, tz *time.Location, t1 time.Time, t2 time.Time) []string {
	t.GetTimestampsCalled = true
	return []string{}
}
