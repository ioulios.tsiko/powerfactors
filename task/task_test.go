package task_test

import (
	"reflect"
	"testing"
	"time"

	"gitlab.com/ioulios.tsiko/powerfactors/task"
)

var (
	oneHour  []string
	oneDay   []string
	oneMonth []string
	oneYear  []string
)

func TestGetTimestamps(t *testing.T) {
	tsk := task.NewTask()
	initializeResults()
	tz, _ := time.LoadLocation("Europe/Athens")

	t.Run("get ptlist 1h", func(t *testing.T) {
		period := task.Period{Type: "h", Value: 1}
		t1 := time.Date(2021, 07, 14, 20, 46, 03, 0, time.UTC)
		t2 := time.Date(2021, 07, 15, 12, 34, 56, 0, time.UTC)
		got := tsk.GetTimestamps(period, tz, t1, t2)

		assertEqual(t, got, oneHour)
	})

	t.Run("get ptlist 1d", func(t *testing.T) {
		period := task.Period{Type: "d", Value: 1}
		t1 := time.Date(2021, 10, 10, 20, 46, 03, 0, time.UTC)
		t2 := time.Date(2021, 11, 15, 12, 34, 56, 0, time.UTC)
		got := tsk.GetTimestamps(period, tz, t1, t2)

		assertEqual(t, got, oneDay)
	})

	t.Run("get ptlist 1mo", func(t *testing.T) {
		period := task.Period{Type: "mo", Value: 1}
		t1 := time.Date(2021, 02, 14, 20, 46, 03, 0, time.UTC)
		t2 := time.Date(2021, 11, 15, 12, 34, 56, 0, time.UTC)
		got := tsk.GetTimestamps(period, tz, t1, t2)

		assertEqual(t, got, oneMonth)
	})

	t.Run("get ptlist 1y", func(t *testing.T) {
		period := task.Period{Type: "y", Value: 1}
		t1 := time.Date(2018, 02, 14, 20, 46, 03, 0, time.UTC)
		t2 := time.Date(2021, 11, 15, 12, 34, 56, 0, time.UTC)
		got := tsk.GetTimestamps(period, tz, t1, t2)

		assertEqual(t, got, oneYear)
	})

	t.Run("get ptlist with t2-t1 < period", func(t *testing.T) {
		period := task.Period{Type: "m", Value: 1}
		t1 := time.Date(2021, 07, 14, 20, 46, 03, 0, time.UTC)
		t2 := time.Date(2021, 07, 14, 12, 34, 56, 0, time.UTC)
		got := tsk.GetTimestamps(period, tz, t1, t2)

		assertEqual(t, got, []string{})
	})
}

func assertEqual(t testing.TB, got, want interface{}) {
	t.Helper()
	if !reflect.DeepEqual(got, want) {
		t.Errorf("wanted %q \nbut got %q", want, got)
	}
}

func initializeResults() {
	oneHour = []string{
		"20210714T210000Z",
		"20210714T220000Z",
		"20210714T230000Z",
		"20210715T000000Z",
		"20210715T010000Z",
		"20210715T020000Z",
		"20210715T030000Z",
		"20210715T040000Z",
		"20210715T050000Z",
		"20210715T060000Z",
		"20210715T070000Z",
		"20210715T080000Z",
		"20210715T090000Z",
		"20210715T100000Z",
		"20210715T110000Z",
		"20210715T120000Z",
	}

	oneDay = []string{
		"20211010T210000Z",
		"20211011T210000Z",
		"20211012T210000Z",
		"20211013T210000Z",
		"20211014T210000Z",
		"20211015T210000Z",
		"20211016T210000Z",
		"20211017T210000Z",
		"20211018T210000Z",
		"20211019T210000Z",
		"20211020T210000Z",
		"20211021T210000Z",
		"20211022T210000Z",
		"20211023T210000Z",
		"20211024T210000Z",
		"20211025T210000Z",
		"20211026T210000Z",
		"20211027T210000Z",
		"20211028T210000Z",
		"20211029T210000Z",
		"20211030T210000Z",
		"20211031T220000Z",
		"20211101T220000Z",
		"20211102T220000Z",
		"20211103T220000Z",
		"20211104T220000Z",
		"20211105T220000Z",
		"20211106T220000Z",
		"20211107T220000Z",
		"20211108T220000Z",
		"20211109T220000Z",
		"20211110T220000Z",
		"20211111T220000Z",
		"20211112T220000Z",
		"20211113T220000Z",
		"20211114T220000Z",
	}

	oneMonth = []string{
		"20210228T220000Z",
		"20210331T210000Z",
		"20210430T210000Z",
		"20210531T210000Z",
		"20210630T210000Z",
		"20210731T210000Z",
		"20210831T210000Z",
		"20210930T210000Z",
		"20211031T220000Z",
	}

	oneYear = []string{
		"20181231T220000Z",
		"20191231T220000Z",
		"20201231T220000Z",
	}
}
